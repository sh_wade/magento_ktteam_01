<?php
namespace Wade\Crud\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		$installer = $setup;

		$installer->startSetup();

		if(version_compare($context->getVersion(), '2.0.0', '>')) {
			if (!$installer->tableExists('wade_post')) {
				$table = $installer->getConnection()->newTable(
					$installer->getTable('wade_table')
				)
					-        $table = $setup->getConnection()->newTable(
            $setup->getTable('wade_table')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Name'
        )->addColumn(
            'Description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Description Text'
        )
	->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'status Text'
        )
	->setComment(
            'Just a Table'
        )
	->addColumn(
		'updated_at',
		\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
		null,
		['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
		'Updated At')
	->setComment('Post Table');
	$installer->getConnection()->createTable($table);
	$installer->getConnection()->addIndex(
	$installer->getTable('wade_table'),
	$setup->getIdxName(
		$installer->getTable('wade_table'),
		['name','Description','status'],
		\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT),
		['name','Description','status'],
		\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
		);
	}
	}

	$installer->endSetup();
	}
}
