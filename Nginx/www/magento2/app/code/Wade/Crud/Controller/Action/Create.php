<?php
 
namespace Wade\Crud\Controller\Action;
 
use Magento\Framework\App\Action\Context;
 
class Create extends \Magento\Framework\App\Action\Action
{
	protected $scopeConfig;
	protected $_postFactory;
	public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
	\Wade\Crud\Model\PostFactory $postFactory
    )
    {
        $this->scopeConfig = $scopeConfig;
 	$this->_postFactory = $postFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $params         = $this->getRequest()->getParams();
	$data = $this->_postFactory->create();    
	//echo print_r($params);
	$data->addData($params);
	$data->save();
 
       return $resultRedirect->setPath('crud/');
    }
}
