<?php
namespace Wade\Crud\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'wade_table_collection';
	protected $_eventObject = 'table_collection';

	protected function _construct()
	{
		$this->_init('Wade\Crud\Model\Post', 'Wade\Crud\Model\ResourceModel\Post');
	}

}
