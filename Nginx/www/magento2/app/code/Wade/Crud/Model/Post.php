<?php
 
namespace Wade\Crud\Model;
 
class Post extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Wade\Crud\Model\ResourceModel\Post');
    }
}
