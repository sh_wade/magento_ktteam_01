<?php
namespace Wade\Crud\Block;
class TestClass extends \Magento\Framework\View\Element\Template{
	protected $_pageFactory;
	protected $_postFactory;
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\Wade\Crud\Model\PostFactory $postFactory){
		$this->_pageFactory = $pageFactory;
		$this->_postFactory = $postFactory;
		return parent::__construct($context);
	}
	protected function _prepareLayout(){
		parent::_prepareLayout();
		$this->pageConfig->getTitle()->set(__('HELLO WORLD'));
		if ($this->getModelData()) {
			$pager = $this->getLayout()->createBlock('Magento\Theme\Block\Html\Pager')
				->setAvailableLimit(array(5=>5))->setShowPerPage(true)->setCollection($this->getModelData());
        		$this->setChild('pager', $pager);
        		$this->getModelData()->load();
		}
		return $this;
	}

	public function getPagerHtml(){
		return $this->getChildHtml('pager');
	}
	public function getModelData(){
		$this->_isScopePrivate = true;
		$page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;		
		$data = $this->_postFactory->create();
		$collection = $data->getCollection(); 
        	$collection->setPageSize(5);
        	$collection->setCurPage($page);      
        	return $collection;
	}

}
?>


